<?php

namespace App\Utility;


use \Core\View;

class Flash {

    public static function danger($message)
    {
        // Code pour afficher le message d'erreur avec la classe "danger"
        echo "<div class='alert alert-danger'>$message</div>";
        //View::renderTemplate('User/login.html');
    }
}
