<?php

namespace App\Controllers;

use App\Config;
use App\Model\UserRegister;
use App\Models\Articles;
use App\Utility\Hash;
use App\Utility\Flash;
use App\Utility\Session;
use \Core\View;
use Exception;
use http\Env\Request;
use http\Exception\InvalidArgumentException;

/**
 * User controller
 */
class User extends \Core\Controller
{

    /**
     * Affiche la page de login
     */
    public function loginAction()
    {
        if(isset($_POST['submit'])){
            $f = $_POST;

            // TODO: Validation
            // Si login OK, redirige vers le compte
            $login = $this->login($f);
            if($login !== NULL){
                header('Location: /account');
            }
            
        }

        View::renderTemplate('User/login.html');
    }

    /**
     * Page de création de compte
     */
    public function registerAction()
    {
        if(isset($_POST['submit'])){
            $f = $_POST;

            if($f['password'] !== $f['password-check']){
                // TODO: Gestion d'erreur côté utilisateur
            } 

            // validation

            $register = $this->register($f);

            if($register !== NULL){
                $this->login($f);
                View::renderTemplate('Home/index.html');
            }
            // TODO: Rappeler la fonction de login pour connecter l'utilisateur
            
        }

        View::renderTemplate('User/register.html');
        
    }

    /**
     * Affiche la page du compte
     */
    public function accountAction()
    {
        if (isset($_POST['delete']))
        {
            $id = $_POST['delete'];
            Articles::deleteById($id); 
        }
        $articles = Articles::getByUser($_SESSION['user']['id']);
        View::renderTemplate('User/account.html', [
            'articles' => $articles
        ]);
        
    }

    public function updateAction()
    {
        
        if (isset($_POST['save']))
        {
            $id = $_POST['save'];
            $newName = $_POST['name'];
            $newDescription = $_POST['description'];
            Articles::update($id, $newName, $newDescription);
            header("location: /account");
        }
        $id = $_GET["product_id"]; 
        $article = Articles::getOne($id);
        View::renderTemplate('User/Update.html', [
            'article' => $article[0],
        ]);
    }  

    /*
     * Fonction privée pour enregister un utilisateur
     */
    private function register($data)
    {
        try {
            // Generate a salt, which will be applied to the during the password
            // hashing process.
            $salt = Hash::generateSalt(32);

            $user = \App\Models\User::getByLogin($data['email']);

            if($user === false){
                $userID = \App\Models\User::createUser([
                    "email" => $data['email'],
                    "username" => $data['username'],
                    "password" => Hash::generate($data['password'], $salt),
                    "salt" => $salt
                ]);
    
                return $userID;
                 
            } else {
                throw new Exception('Ce mail existe déjà !');
            }
  

        } catch (Exception $ex) {
            // TODO : Set flash if error : utiliser la fonction en dessous
            Flash::danger($ex->getMessage());
        }
    }

    private function login($data){
        try {
            if(!isset($data['email'])){
                throw new Exception('TODO');
            }

            $user = \App\Models\User::getByLogin($data['email']);

            if($user === false){
                throw new Exception('Identifiant incorrect');
            }

            if (Hash::generate($data['password'], $user['salt']) !== $user['password']) {
                //return false;
                throw new Exception('Identifiant incorrect');
            }

            // TODO: Create a remember me cookie if the user has selected the option
            // to remained logged in on the login form.
            // https://github.com/andrewdyer/php-mvc-register-login/blob/development/www/app/Model/UserLogin.php#L86

            $_SESSION['user'] = array(
                'id' => $user['id'],
                'username' => $user['username'],
                'email' => $user['email'],
            );

            return true;

        } catch (Exception $ex) {
            // TODO : Set flash if error
            Flash::danger($ex->getMessage());
            //View::renderTemplate('User/login.html');
        }
    }


    /**
     * Logout: Delete cookie and session. Returns true if everything is okay,
     * otherwise turns false.
     * @access public
     * @return boolean
     * @since 1.0.2
     */
    public function logoutAction() {

        /*
        if (isset($_COOKIE[$cookie])){
            // TODO: Delete the users remember me cookie if one has been stored.
            // https://github.com/andrewdyer/php-mvc-register-login/blob/development/www/app/Model/UserLogin.php#L148
        }*/
        // Destroy all data registered to the session.

        $_SESSION = array();

        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
            );
        }

        session_destroy();

        header ("Location: /");

        return true;
    }

}
